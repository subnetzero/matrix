# Summary
This document describes how to connect to the Subnet Zero Matrix Chat

## URLs
The primary URL is https://matrix.subnetzero.io. You can reach the bundled web client to register by going to https://matrix.subnetzero.io/_matrix/client

## Identity Providers
All three default IDPs are enabled, so if you have an existing account, you can use it. You may also click `Create Account` and sign up. You will receive an e-mail with a verification link.

## Federation
The server supports federation, so you can join federated channels.

## Hosting/Backend
Currently the matrix server is hosted in Digital Ocean on a 2CPU 2GB box. Nginx functions as a reverse proxy for clients; the Federation port is exposed directly to the Internet, as recommended by the Matrix guides.

## Clients
1. You can go to https://riot.im/app/ and sign in via the web client
2. On macOS, there is a non-Electron client: https://neilalexander.eu/seaglass
3. You can download the standard Desktop client here: https://riot.im/desktop.html

## Security
Nginx is configured with auto-renewing Let's Encrypt certs. Backups of the VM are made daily.
